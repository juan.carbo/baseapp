﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using Redjar.BaseApp.Application.Mediators.PersonOperations.Get;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.Entities;
using Redjar.BaseApp.Domain.Repositories;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.Tests.Mediators.PersonOperations.Get
{
    public class GetPersonHandlerTests
    {
        private GetPersonHandler personHandler;
        Mock<IGenericRepository<Persona>> personRepository;
        private Mock<IMapper> autoMapper;

        [SetUp]
        public void SetUp()
        {
            personRepository = new Mock<IGenericRepository<Persona>>();
            autoMapper = new Mock<IMapper>();
            personHandler = new GetPersonHandler(personRepository.Object, autoMapper.Object);
        }
        [Test]
        public async Task GetPersonShouldWork()
        {
            //Arrange
            GetPersonRequest request = new GetPersonRequest(Guid.NewGuid());
            var person = new Persona();
            personRepository.Setup(x => x.Find(It.IsAny<Guid>())).Returns(person);
            var personDto = new PersonaDto();
            autoMapper.Setup(x => x.Map<PersonaDto>(It.IsAny<Persona>())).Returns(personDto);
            //Act
            var result = await personHandler.Handle(request, new System.Threading.CancellationToken());
            //Assert
            personRepository.Verify(x => x.Find(It.IsAny<Guid>()), Times.Once());
            Assert.AreEqual(result.Entity, personDto);
            Assert.IsTrue(result.Valid);
        }
        [Test]
        public async Task GetPersonWithInvalidRequestShouldReturn404()
        {
            //Arrange
            GetPersonRequest request = new GetPersonRequest(Guid.Empty);
            //Act
            var result = await personHandler.Handle(request, new System.Threading.CancellationToken());
            //Assert
            personRepository.Verify(x => x.Find(It.IsAny<Guid>()), Times.Never());
            autoMapper.Verify(x => x.Map<PersonaDto>(It.IsAny<Persona>()), Times.Never());
            Assert.AreEqual(HttpStatusCode.BadRequest, result.Status);
            Assert.IsTrue(result.Invalid);
        }
    }
}
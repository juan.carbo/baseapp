﻿using Microsoft.AspNetCore.Mvc;
using Redjar.BaseApp.Domain.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Api.Presenters
{
    public class BasePresenter : IBasePresenter
    {
        public IActionResult GenerateResponse(Result result)
        {
            if (result.Valid)
            {
                return new OkResult();
            }
            else
            {
                return GenerateErrorResponse(result);
            }
            throw new NotImplementedException();
        }

        private IActionResult GenerateErrorResponse(Result result)
        {
            return new ObjectResult(result.Notifications) { StatusCode = (int)result.Status };
        }

        public IActionResult GenerateResponse<T>(EntityResult<T> result) where T : class
        {
            if (result.Valid)
            {
                return new OkObjectResult(result.Entity);
            }
            else
            {
                return GenerateErrorResponse(result);
            }
        }
    }
}

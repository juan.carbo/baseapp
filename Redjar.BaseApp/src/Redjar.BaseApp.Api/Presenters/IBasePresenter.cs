﻿using Microsoft.AspNetCore.Mvc;
using Redjar.BaseApp.Domain.ResponseModel;

namespace Redjar.BaseApp.Api.Presenters
{
    public interface IBasePresenter
    {
        IActionResult GenerateResponse(Result result);
        IActionResult GenerateResponse<T>(EntityResult<T> result) where T : class;
    }
}
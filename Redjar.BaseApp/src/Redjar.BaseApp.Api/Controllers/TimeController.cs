﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Redjar.BaseApp.Api.Presenters;
using Redjar.BaseApp.Application.Mediators.TimeOperations.Get;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBasePresenter _presenter;

        public TimeController(IMediator mediator, IBasePresenter presenter)
        {
            _mediator = mediator;
            _presenter = presenter;
        }

        [HttpGet]
        public async Task<IActionResult> GetTime()
        {
            var result = await _mediator.Send(new GetTimeRequest());
            return _presenter.GenerateResponse(result);
        }
    }
}

﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Redjar.BaseApp.Api.Presenters;
using Redjar.BaseApp.Application.Mediators.CategoriaOperations.Insert;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.Entities;
using Redjar.BaseApp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriasController: ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBasePresenter _presenter;

        public CategoriasController(IMediator mediator, IBasePresenter presenter)
        {
            _mediator = mediator;
            _presenter = presenter;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoriaDto categoriaDto)
        {
            var result = await _mediator.Send(new InsertCategoriaRequest(categoriaDto));
            return _presenter.GenerateResponse(result);
        }
    }
}

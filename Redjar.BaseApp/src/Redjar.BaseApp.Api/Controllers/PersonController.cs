﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Redjar.BaseApp.Api.Presenters;
using Redjar.BaseApp.Application.Mediators.PersonOperations.Get;
using Redjar.BaseApp.Application.Mediators.PersonOperations.Insert;
using Redjar.BaseApp.Domain.Dto;
using System;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBasePresenter _presenter;

        public PersonController(IMediator mediator, IBasePresenter presenter)
        {
            _mediator = mediator;
            _presenter = presenter;
        }
        [HttpPost]
        public async Task<IActionResult> CreatePerson(PersonaDto person)
        {
            var result = await _mediator.Send(new InsertPersonRequest(person ));
            return _presenter.GenerateResponse(result);
        }
        [HttpGet("id")]
        public async Task<IActionResult> GetPerson(Guid id)
        {
            var result = await _mediator.Send(new GetPersonRequest(id));
            return _presenter.GenerateResponse(result);
        }
    }
}

﻿using AutoMapper;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.Entities;

namespace Redjar.BaseApp.Api.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<PersonaDto, Persona>().ReverseMap();
            CreateMap<CategoriaDto, Categoria>().ReverseMap();
            CreateMap<LineaDeVentaDto, LineaDeVenta>().ReverseMap();
            CreateMap<ProductoDto, Producto>().ReverseMap();
            CreateMap<VentaDto, Venta>().ReverseMap();
        }
    }
}

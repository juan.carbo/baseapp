﻿namespace Redjar.BaseApp.Application.APIs.WorldClock
{
    public class WorldClockApiTime
    {
        public string id { get; set; }
        public string currentDateTime { get; set; }
        public string utcOffset { get; set; }
        public bool isDayLightSavingsTime { get; set; }
        public string dayOfTheWeek { get; set; }
        public string timeZoneName { get; set; }
        public long currentFileTime { get; set; }
        public string ordinalDate { get; set; }
        public object serviceResponse { get; set; }
    }

}

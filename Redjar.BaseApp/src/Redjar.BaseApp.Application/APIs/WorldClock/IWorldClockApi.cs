﻿using Refit;
using System;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.APIs.WorldClock
{
    public interface IWorldClockApi
    {
        [Get("/api/json/utc/now")]
        Task<WorldClockApiTime> getUtcTime();
    }
}

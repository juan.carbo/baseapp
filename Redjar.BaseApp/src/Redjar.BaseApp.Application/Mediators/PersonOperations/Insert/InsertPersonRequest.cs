﻿using Flunt.Notifications;
using Flunt.Validations;
using MediatR;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.ResponseModel;
using System;

namespace Redjar.BaseApp.Application.Mediators.PersonOperations.Insert
{
    public class InsertPersonRequest: Notifiable, IRequest<Result>
    {
        public InsertPersonRequest(PersonaDto person)
        {
            Person = person;
            AddNotifications(new Contract()
                .IsEmpty(Person.Id, "Id", "El id de la persona no puede ser predefinido")
                .IsNotNullOrEmpty(Person.Name, "Name", "La persona necesita un nombre")
                .IsNotNullOrEmpty(Person.LastName, "LastName", "La persona necesita un apellido")
                .IsBetween(Person.BirthDate, new System.DateTime(1900, 01,01), DateTime.Now, "BirthDate", "La fecha de cumpleaños está fuera de los limites aceptables"));
        }
        public PersonaDto Person { get; } //Todo validate
    }
}

﻿using AutoMapper;
using MediatR;
using Redjar.BaseApp.Domain.Entities;
using Redjar.BaseApp.Domain.Repositories;
using Redjar.BaseApp.Domain.ResponseModel;
using System.Threading;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.Mediators.PersonOperations.Insert
{
    public class InsertPersonHandler : IRequestHandler<InsertPersonRequest, Result>
    {
        private readonly IGenericRepository<Persona> _personRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public InsertPersonHandler(IGenericRepository<Persona> personRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _personRepository = personRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result> Handle(InsertPersonRequest request, CancellationToken cancellationToken)
        {
            if (request.Valid)
            {
                _personRepository.Insert(_mapper.Map<Persona>(request.Person));
                await _unitOfWork.SaveChangesAsync();

                return new Result(request.Notifications, System.Net.HttpStatusCode.Created);
            }

            else return new Result(request.Notifications, System.Net.HttpStatusCode.BadRequest);
        }
    }
}

﻿using AutoMapper;
using MediatR;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.Entities;
using Redjar.BaseApp.Domain.Repositories;
using Redjar.BaseApp.Domain.ResponseModel;
using System.Threading;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.Mediators.PersonOperations.Get
{
    public class GetPersonHandler : IRequestHandler<GetPersonRequest, EntityResult<PersonaDto>>
    {
        private readonly IGenericRepository<Persona> _personRepository;
        private readonly IMapper _mapper;

        public GetPersonHandler(IGenericRepository<Persona> personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }

        public async Task<EntityResult<PersonaDto>> Handle(GetPersonRequest request, CancellationToken cancellationToken)
        {
            if (request.Valid)
                return new EntityResult<PersonaDto>(
                    _mapper.Map<PersonaDto>(await Task.Run(() => _personRepository.Find(request.Id))),
                    request.Notifications,
                    System.Net.HttpStatusCode.OK);
            else
            {
                return new EntityResult<PersonaDto>(null, request.Notifications, System.Net.HttpStatusCode.BadRequest);
            }
        }
    }
}

﻿using Flunt.Notifications;
using Flunt.Validations;
using MediatR;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.ResponseModel;
using System;

namespace Redjar.BaseApp.Application.Mediators.PersonOperations.Get
{
    public class GetPersonRequest : Notifiable, IRequest<EntityResult<PersonaDto>>
    {
        public GetPersonRequest(Guid id)
        {
            Id = id;
            AddNotifications(new Contract()
                .IsNotEmpty(Id, "Id", "El campo Id no puede estar vacio en una busqueda"));
        }
        public Guid Id { get; }
    }
}

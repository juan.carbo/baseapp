﻿using Flunt.Notifications;
using Flunt.Validations;
using MediatR;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.ResponseModel;

namespace Redjar.BaseApp.Application.Mediators.CategoriaOperations.Insert
{
    public class InsertCategoriaRequest: Notifiable, IRequest<EntityResult<CategoriaDto>>
    {
        public InsertCategoriaRequest(CategoriaDto categoriaDto)
        {
            Categoria = categoriaDto;
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(categoriaDto.Descripcion, "Descripcion", "La descripcion no puede estar vacia o ser null")
                .IsNotNullOrEmpty(categoriaDto.Valor, "Valor", "El valor no puede estar vacio o ser null")
                .IsEmpty(categoriaDto.Id, "Id", "No puede insertarse un item con id predefinido"));
        }
        public CategoriaDto Categoria { get; }
    }
}

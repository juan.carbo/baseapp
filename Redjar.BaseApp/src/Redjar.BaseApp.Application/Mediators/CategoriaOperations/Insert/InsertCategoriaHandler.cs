﻿using AutoMapper;
using MediatR;
using Redjar.BaseApp.Domain.Dto;
using Redjar.BaseApp.Domain.Entities;
using Redjar.BaseApp.Domain.Repositories;
using Redjar.BaseApp.Domain.ResponseModel;
using System.Threading;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.Mediators.CategoriaOperations.Insert
{
    public class InsertCategoriaHandler : IRequestHandler<InsertCategoriaRequest, EntityResult<CategoriaDto>>
    {
        private readonly IGenericRepository<Categoria> _categoriasRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public InsertCategoriaHandler(IGenericRepository<Categoria> categoriasRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _categoriasRepository = categoriasRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<EntityResult<CategoriaDto>> Handle(InsertCategoriaRequest request, CancellationToken cancellationToken)
        {
            if (request.Invalid)
            {
                return new EntityResult<CategoriaDto>(null, request.Notifications, System.Net.HttpStatusCode.BadRequest);
            }
            request.Categoria.Productos = null;
            var categoria = _mapper.Map<Categoria>(request.Categoria);
            _categoriasRepository.Insert(categoria);
            await _unitOfWork.SaveChangesAsync();

            return new EntityResult<CategoriaDto>(_mapper.Map<CategoriaDto>(categoria), request.Notifications, System.Net.HttpStatusCode.OK);
        }
    }
}

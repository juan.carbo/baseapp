﻿using MediatR;
using Newtonsoft.Json;
using Redjar.BaseApp.Application.APIs.WorldClock;
using Redjar.BaseApp.Domain.ResponseModel;
using Refit;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Application.Mediators.TimeOperations.Get
{
    public class GetTimeHandler : IRequestHandler<GetTimeRequest, EntityResult<GetTimeResponse>>
    {
        private readonly IWorldClockApi _worldClockApi;

        public GetTimeHandler(IWorldClockApi worldClockApi)
        {
            _worldClockApi = worldClockApi;
        }

        public async Task<EntityResult<GetTimeResponse>> Handle(GetTimeRequest request, CancellationToken cancellationToken)
        {
            if (request.Valid)
            {
                var result = await _worldClockApi.getUtcTime();
                return new EntityResult<GetTimeResponse>(
                    new GetTimeResponse { Time = result },
                    request.Notifications, 
                    System.Net.HttpStatusCode.OK);
            }
            return new EntityResult<GetTimeResponse>(null, request.Notifications, System.Net.HttpStatusCode.BadRequest);
        }
    }
}

﻿using Flunt.Notifications;
using MediatR;
using Redjar.BaseApp.Domain.ResponseModel;
using System.Collections.Generic;
using System.Text;

namespace Redjar.BaseApp.Application.Mediators.TimeOperations.Get
{
    public class GetTimeRequest : Notifiable ,IRequest<EntityResult<GetTimeResponse>>
    {
    }
}

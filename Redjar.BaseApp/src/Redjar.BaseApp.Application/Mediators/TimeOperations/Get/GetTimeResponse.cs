﻿using Redjar.BaseApp.Application.APIs.WorldClock;

namespace Redjar.BaseApp.Application.Mediators.TimeOperations.Get
{
    public class GetTimeResponse
    {
        public WorldClockApiTime Time { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Redjar.BaseApp.Domain.Entities
{
    public class Categoria
    {
        public Guid Id { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public IEnumerable<Producto> Productos { get; set; }
    }
}

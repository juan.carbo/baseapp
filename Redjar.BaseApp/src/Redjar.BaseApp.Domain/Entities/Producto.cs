﻿using System;
using System.Collections.Generic;

namespace Redjar.BaseApp.Domain.Entities
{
    public class Producto
    {
        public Guid Id { get; set; }
        public float Precio { get; set; }
        public string Descripcion { get; set; }
        public int Stock { get; set; }
        public Guid CategoriaId { get; set; }
        public Categoria Categoria { get; set; }
        public IEnumerable<LineaDeVenta> LineasDeVenta { get; set; }
    }
}

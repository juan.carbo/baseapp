﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Redjar.BaseApp.Domain.Entities
{
    public class Persona
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public IEnumerable<Venta> Ventas { get; set; }
        public IEnumerable<Venta> Compras { get; set; }
    }
}
﻿using System;

namespace Redjar.BaseApp.Domain.Entities
{
    public class LineaDeVenta
    {
        public Guid Id { get; set; }
        public Guid ProductoId { get; set; }
        public Producto Producto { get; set; }
        public Guid VentaId { get; set; }
        public Venta Venta { get; set; }
        public int Cantidad { get; set; }
        public float SubTotal { get; set; }
    }
}

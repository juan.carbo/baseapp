﻿using System;
using System.Collections.Generic;

namespace Redjar.BaseApp.Domain.Entities
{
    public class Venta
    {
        public Guid Id { get; set; }
        public DateTime FechaHora { get; set; }
        public Guid? CajeroId { get; set; }
        public Persona Cajero { get; set; }
        public Guid? ClienteId { get; set; }
        public Persona Cliente { get; set; }
        public IEnumerable<LineaDeVenta> LineasDeVenta { get; set; }
    }
}

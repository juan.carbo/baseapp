﻿using Flunt.Notifications;
using System.Collections.Generic;
using System.Net;

namespace Redjar.BaseApp.Domain.ResponseModel
{
    public class EntityResult<T> : Result where T : class
    {
        public EntityResult(T entity, IReadOnlyCollection<Notification> notifications, HttpStatusCode status) : base(notifications, status)
        {
            Entity = entity;
        }

        public T Entity { get; }
    }
}

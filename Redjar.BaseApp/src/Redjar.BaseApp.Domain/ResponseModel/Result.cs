﻿using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Redjar.BaseApp.Domain.ResponseModel
{
    public class Result : Notifiable
    {
        public Result(IReadOnlyCollection<Notification> notifications, HttpStatusCode status)
        {
            this.AddNotifications(notifications);
            Status = status;
        }

        public void AddNotification(string error)
        {
            this.AddNotification(null, error);
        }

        public void AddNotification(string error, HttpStatusCode errorCode)
        {
            this.AddNotification(null, error);
            this.Status = errorCode;
        }

        public void AddNotification(string property, string message, HttpStatusCode errorCode)
        {
            this.AddNotification(property, message);
            this.Status = errorCode;
        }

        public void AddNotification(Notification notification, HttpStatusCode errorCode)
        {
            this.AddNotification(notification);
            this.Status = errorCode;
        }

        public void AddNotifications(IReadOnlyCollection<Notification> notifications, HttpStatusCode errorCode)
        {
            this.AddNotifications(notifications);
            this.Status = errorCode;
        }

        public HttpStatusCode? Status { get; set; }
    }
}

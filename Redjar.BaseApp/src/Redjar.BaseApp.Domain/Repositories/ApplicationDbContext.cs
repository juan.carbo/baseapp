﻿using Microsoft.EntityFrameworkCore;
using Redjar.BaseApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Domain.Repositories
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        DbSet<Persona> People { get; set; }
        DbSet<Producto> Productos { get; set; }
        DbSet<LineaDeVenta> LineasDeVenta { get; set; }
        DbSet<Categoria> Categorias { get; set; }
        DbSet<Venta> Ventas { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Categoria>().HasMany(cat => cat.Productos).WithOne(prd => prd.Categoria).HasForeignKey(prd => prd.CategoriaId);
            modelBuilder.Entity<Producto>().HasMany(prd => prd.LineasDeVenta).WithOne(ldv => ldv.Producto).HasForeignKey(ldv => ldv.ProductoId);
            modelBuilder.Entity<Venta>().HasMany(ven => ven.LineasDeVenta).WithOne(ldv => ldv.Venta).HasForeignKey(ldv => ldv.VentaId);
            modelBuilder.Entity<Venta>().HasOne(ven => ven.Cajero).WithMany(per => per.Ventas).HasForeignKey(ven => ven.CajeroId).OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Venta>().HasOne(ven => ven.Cliente).WithMany(per => per.Compras).HasForeignKey(ven => ven.ClienteId).OnDelete(DeleteBehavior.NoAction);
        }
    }
}

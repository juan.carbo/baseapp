﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Redjar.BaseApp.Domain.Repositories
{
    public interface IUnitOfWork
    {
        void SaveChanges();
        Task SaveChangesAsync();
    }
}

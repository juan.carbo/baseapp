﻿using System;
using System.Collections.Generic;

namespace Redjar.BaseApp.Domain.Dto
{
    public class VentaDto
    {
        public Guid Id { get; set; }
        public DateTime FechaHora { get; set; }
        public Guid CajeroId { get; set; }
        public PersonaDto Cajero { get; set; }
        public Guid ClienteId { get; set; }
        public PersonaDto Cliente { get; set; }
        public IEnumerable<LineaDeVentaDto> LineasDeVenta { get; set; }

    }
}

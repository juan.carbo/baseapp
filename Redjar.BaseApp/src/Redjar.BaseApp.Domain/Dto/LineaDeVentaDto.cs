﻿using System;

namespace Redjar.BaseApp.Domain.Dto
{
    public class LineaDeVentaDto
    {
        public Guid Id { get; set; }
        public Guid ProductoId { get; set; }
        public ProductoDto Producto { get; set; }
        public Guid VentaId { get; set; }
        public VentaDto Venta { get; set; }
        public int Cantidad { get; set; }
        public float SubTotal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Redjar.BaseApp.Domain.Dto
{
    public class ProductoDto
    {
        public Guid Id { get; set; }
        public float Precio { get; set; }
        public string Descripcion { get; set; }
        public int Stock { get; set; }
        public Guid CategoriaId { get; set; }
        public CategoriaDto Categoria { get; set; }
        public IEnumerable<LineaDeVentaDto> LineasDeVenta { get; set; }
    }
}

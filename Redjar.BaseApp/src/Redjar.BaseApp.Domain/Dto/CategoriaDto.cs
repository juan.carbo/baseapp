﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Redjar.BaseApp.Domain.Dto
{
    public class CategoriaDto
    {
        public Guid Id { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public IEnumerable<ProductoDto> Productos { get; set; }
    }
}

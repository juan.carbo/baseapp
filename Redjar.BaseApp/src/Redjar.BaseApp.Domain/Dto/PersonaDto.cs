﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Redjar.BaseApp.Domain.Dto
{
    public class PersonaDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
